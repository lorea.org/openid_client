<?php

global $MIGRATED;

$local_version = (int)elgg_get_plugin_setting('version', 'openid_client');
if (2012022501 <= $local_version) {
        error_log("Openid client requires no upgrade");
        // no upgrade required
        return;
}

/**
 * Save previous author id
 */
function openid_client_user_2012100501($user) {
	$MIGRATED += 1;
	if ($MIGRATED % 100 == 0) {
		error_log(" * openid user $user->guid");
	}

	if ($user->alias) {
		$alias = $user->alias;
		if (strpos($alias, 'pg/profile') !== FALSE) {
			$alias = str_replace('pg/profile', 'profile', $alias);
		}
		error_log($alias);
		$user->annotate('openid_identifier', $alias, ACCESS_PUBLIC);
	}
	return true;
}



/*
 * Run upgrade. First users, then pads
 */
// users
$options = array('type' => 'user', 'limit' => 0);

$MIGRATED = 0;

$previous_access = elgg_set_ignore_access(true);
$batch = new ElggBatch('elgg_get_entities', $options, "openid_client_user_2012100501", 100);
elgg_set_ignore_access($previous_access);

if ($batch->callbackResult) {
	error_log("Elgg openid users upgrade (201210050) succeeded");
	elgg_set_plugin_setting('version', 2012022501, 'openid_client');
} else {
	error_log("Elgg openid users upgrade (201210050) failed");
}


