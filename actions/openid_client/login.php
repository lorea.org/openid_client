<?php
/**
 * OpenID client login action
 */

elgg_load_library('openid_consumer');

$provider = get_input('openid_provider');
$persistent = get_input('persistent', false);

if ($provider == 'others') {
	$provider = get_input('openid_url');
}

$consumer = new ElggOpenIDConsumer($store);
$consumer->setURL($provider);
$consumer->setReturnURL(elgg_get_site_url() . "mod/openid_client/return.php?persistent=$persistent");

$html = $consumer->requestAuthentication();
if ($html) {
	echo $html;
	exit;
} else {
	$flipped_providers = array_flip(elgg_get_config('openid_providers'));
	if (isset($flipped_providers[$provider])) {
		$provider_name = $flipped_providers[$provider];
	} else {
		$provider_name = $provider;
	}
	register_error(elgg_echo('openid_client:error:no_html', array($provider_name)));
	forward();
}
