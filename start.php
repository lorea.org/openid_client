<?php
/**
 * Elgg OpenID client
 *
 * This is a rewrite of the OpenID client written by Kevin Jardine for
 * Curverider Ltd for Elgg 1.0-1.7.
 */

elgg_register_event_handler('init', 'system', 'openid_client_init');

/**
 * OpenID client initialization
 */
function openid_client_init() {
	elgg_extend_view('css/elgg', 'openid_client/css');
	elgg_extend_view('js/elgg', 'openid_client/js');

	elgg_register_plugin_hook_handler('register', 'menu:openid_login', 'openid_client_setup_menu');
	
	$base = elgg_get_plugins_path() . 'openid_client/actions/openid_client';
	elgg_register_action('openid_client/login', "$base/login.php", 'public');
	elgg_register_action('openid_client/register', "$base/register.php", 'public');

	$base = elgg_get_plugins_path() . 'openid_client/lib';
	elgg_register_library('openid_client', "$base/helpers.php");
	
	elgg_set_config('openid_providers', array(
		'N-1'			=> 'https://n-1.cc/mod/openid_server/server.php',
		'Ecoxarxes'		=> 'https://cooperativa.ecoxarxes.cat/mod/openid_server/server.php',
                'Anillosur'             => 'https://anillosur.net/mod/openid_server/server.php',
                'Saravea'               => 'https://saravea.net/mod/openid_server/server.php',
                'Enekenbat'             => 'https://enekenbat.cc/mod/openid_server/server.php',
<<<<<<< HEAD
=======

>>>>>>> 84be7867eac3e0b69cca4d84c184f6e8e4d2e43b
		// ...
	));

	// the return to page needs to be public
	elgg_register_plugin_hook_handler('public_pages', 'walled_garden', 'openid_client_public');
	elgg_register_event_handler('upgrade', 'system', 'openid_client_run_upgrades');

}

function openid_client_run_upgrades() {
        if (include_once(elgg_get_plugins_path() . 'upgrade-tools/lib/upgrade_tools.php')) {
                upgrade_module_run('openid_client');
        }

}

/**
 * Set the correct subtype for OpenID users
 *
 * @param ElggUser $user New user
 * @return void
 */
function openid_client_set_subtype($user) {
	$db_prefix = elgg_get_config('dbprefix');
	$guid = (int)$user->getGUID();
	$subtype_id = (int)add_subtype('user', 'openid');

	$query = "UPDATE {$db_prefix}entities SET subtype = $subtype_id WHERE guid = $guid";
	update_data($query);
}

/**
 * Add pages to the list of public pages for walled garden needed for OpenID
 * transaction
 *
 * @param string $hook  Hook name
 * @param string $type  Hook type
 * @param array  $pages Array of public pages
 * @return array
 */
function openid_client_public($hook, $type, $pages) {
	$pages[] = 'action/openid_client/login';
	$pages[] = 'mod/openid_client/return.php';
	$pages[] = 'action/openid_client/register';
	return $pages;
}
