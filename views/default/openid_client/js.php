<?php
/**
 * OpenID JavaScript
 */

?>

// OpenID toggle
elgg.register_hook_handler('init', 'system', function() {
	$('.openid-login-icon').click(function(e) {
		var openid_box = $(this).prev('.elgg-form-openid-client-login');
		var shown = openid_box.is(':visible') ? openid_box : openid_box.prev(); 
		var hidden = !openid_box.is(':visible') ? openid_box : openid_box.prev();
		shown.fadeOut(function() {
			hidden.fadeIn();
		});
		e.preventDefault();
	});
	$(".elgg-form-openid-client-login [name='openid_provider']").change(function(event) {
		if (this.value == 'others') {
			$(".openid-client-url").slideDown().find('input').focus();
		} else {
			$(".openid-client-url").slideUp();
		}
	});
});
