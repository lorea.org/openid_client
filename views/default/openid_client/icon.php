<?php

echo elgg_view('output/url', array(
	'text' => elgg_view_icon('openid') . elgg_echo('openid_client:login'),
	'href' => '',
	'title' => elgg_echo('openid_client'),
	'class' => "openid-login-icon",
));