<?php
/**
 * OpenID client CSS
 */
?>

.elgg-icon-openid {
	background-image: url(<?php echo elgg_get_site_url(); ?>mod/openid_client/graphics/openid_icon.png);
}

.elgg-form-login {
	margin-bottom: 10px;
}