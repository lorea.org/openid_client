<?php
/**
 * OpenID login if username or full url required
 */

$options_values = array_flip(elgg_get_config('openid_providers'));
$options_values['others'] = elgg_echo('openid_client:others');

?>
<div>
<label><?php echo elgg_echo('openid_client:login:header'); ?></label>
<br />
<?php
echo elgg_view('input/dropdown', array(
	'name' => 'openid_provider',
	'options_values' => $options_values,
));
?>
</div>

<div class="openid-client-url hidden">
<label><?php echo elgg_echo('openid_client:url'); ?> </label>

<?php
echo elgg_view('input/text', array('name' => 'openid_url', 'class' => 'mbs'));
?>

</div>

<div class="elgg-foot">
	<label class="mtm float-alt">
		<input type="checkbox" name="persistent" value="true" />
		<?php echo elgg_echo('user:persistent'); ?>
	</label>
	
	<?php echo elgg_view('input/submit', array('value' => elgg_echo('login'))); ?>
	
	<?php 
	if (isset($vars['returntoreferer'])) {
		echo elgg_view('input/hidden', array('name' => 'returntoreferer', 'value' => 'true'));
	}
	?>
</div>